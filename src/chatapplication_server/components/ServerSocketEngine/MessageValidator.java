package chatapplication_server.components.ServerSocketEngine;

import Helpers.CheckSumHelper;
import SocketActionMessages.ChatMessage;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

/**
 * Class used to validate a sent message over the socket connection
 * Timestamp and checksum is current supported in validation
 * */
public class MessageValidator {

    /** Maximum seconds allowed before the message expires */
    private int maxSecondsAllowed = 60;

    public boolean validateMessage(ChatMessage msgObj) throws IOException, NoSuchAlgorithmException {

        if(!msgObj.getValidationEnabled()) {
            /** Return true if validation is not enabled */
            return true;
        }

        // Check the checksum (If the message has been altered)
        String checkSumToValidate = msgObj.getCheckSum();

        /** It need to get the checksum while the checksum variable is not set. (Or they will not match) */
        msgObj.setChecksum(null);
        String newCheckSum = CheckSumHelper.getChecksum(msgObj);
        msgObj.setChecksum(checkSumToValidate);

        if(checkSumToValidate.equals(newCheckSum) == false) {
            // Message has been altered since sent
            System.out.println("The message has been altered");
            return false;
        }

        Long msgTime = msgObj.getTimeStamp().getTime();
        Long systemTime = new Date(System.currentTimeMillis()).getTime();
        Long timePassed = (systemTime - msgTime);

        if(timePassed > maxSecondsAllowed * 1000) {
            System.out.println("The message has expired");
            return false;
        }

        return true;
    }
}
