/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SocketActionMessages;

import Helpers.CheckSumHelper;

import javax.crypto.*;
import java.io.IOException;
import java.io.Serializable;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

/**
 * This class defines the different type of messages that will be exchanged between the
 * Clients and the Server.
 * When talking from a Java Client to a Java Server a lot easier to pass Java objects, no
 * need to count bytes or to wait for a line feed at the end of the frame
 * 
 * @author atgianne
 */
public class ChatMessage implements Serializable 
{
    protected static final long serialVersionUID = 1112122200L;
 
    // The different types of message sent by the Client

    // WHOISIN to receive the list of the users connected

    // MESSAGE an ordinary message

    // LOGOUT to disconnect from the Server

    public static final int WHOISIN = 0, MESSAGE = 1, LOGOUT = 2,PRIVATEMESSAGE = 3, SHAREPUBLICKEY = 4;

    private int type;
    private String message;

    /** Added timestamp, checksum and validationEnabled - Used to validate the message sent over the socket conneciton
     * */
    private Date timeStamp;
    private String checkSum;
    private boolean validationEnabled;

    // constructor
    /** Constructor used to decrypt the encrypted ChatMessage object
     * The message is decrypted using the shared secret key
     *
     * */
    public ChatMessage(SealedObject sealed, SecretKey secretKey) {
        try {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        ChatMessage encryptedObject = (ChatMessage) sealed.getObject(cipher);

        this.type = encryptedObject.type;
        this.message = encryptedObject.message;
        this.timeStamp = encryptedObject.timeStamp;
        this.checkSum = encryptedObject.checkSum;
        this.validationEnabled = encryptedObject.validationEnabled;

        } catch (NoSuchPaddingException | InvalidKeyException | IOException | IllegalBlockSizeException | NoSuchAlgorithmException | BadPaddingException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public ChatMessage(int type, String message) {

        this.type = type;
        this.message = message;
    }

    /** Enables message validation
     * Validation is checked when a message is received either on the server or client side
     * */
    public void AddValidation() throws IOException, NoSuchAlgorithmException {
        this.validationEnabled = true;
        this.timeStamp = new Date(System.currentTimeMillis());
        this.checkSum = CheckSumHelper.getChecksum(this);
    }

    // getters

    public int getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setChecksum(String checkSum) {
        this.checkSum = checkSum;
    }
    public String getCheckSum() {
        return checkSum;
    }

    public boolean getValidationEnabled() {
        return validationEnabled;
    }
}
