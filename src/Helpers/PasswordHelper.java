package Helpers;

import javax.crypto.*;
import java.io.IOException;
import java.io.Serializable;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class PasswordHelper {

    public static SealedObject encryptObject(Serializable o, SecretKey secretKey)
    {
        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            SealedObject sealed = new SealedObject(o, cipher);
            return sealed;
        } catch (NoSuchPaddingException | InvalidKeyException | IOException | IllegalBlockSizeException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return null;
    }
}
